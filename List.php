<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    .container {
        display: flex;
        flex-direction: column;
        height: auto;
        width: 34vw;
        margin: 2rem 33vw;
        border: 2px solid #385e8b;
        padding: 1rem;
    }

    .form-group {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: space-around;
    }

    .form-child {
        display: flex;
        justify-content: space-around;
        align-items: center;
        margin-bottom: 1rem;
    }

    .form-text {
        width: 60px;
    }

    .sub-form-text {
        display: flex;
        width: 180px;
        height: 30px;
    }

    .search-child-input {
        display: flex;
        justify-content: space-around;
        justify-content: center;
        width: 200px;
    }

    .input {
        height: inherit;
        width: inherit;
        border: 2px solid #9cafc6;
    }

    .search {
        display: flex;
        justify-content: center;
    }

    .delete-search {
        color: #cfdded;
        margin-right: 10px;
        background-color: #4f81bd;
        border-radius: 5px;
        border: 2px solid #385e8b;
        height: 30px;
        width: 80px
    }

    .sub-search {
        color: #cfdded;
        background-color: #4f81bd;
        border-radius: 5px;
        border: 2px solid #385e8b;
        height: 30px;
        width: 80px
    }

    .amount {
        display: flex;
        justify-content: flex-start;
    }

    .add {
        display: flex;
        justify-content: flex-end;
    }

    .sub-add {
        color: #cfdded;
        background-color: #4f81bd;
        border-radius: 5px;
        border: 2px solid #385e8b;
        height: 30px;
        width: 80px
    }

    .list {
        display: flex;
        flex-direction: column;
    }

    .sub-list {
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 6px;
    }

    .sub-list-item {
        display: flex;
        width: 360px;
        justify-content: space-between;
    }

    .sub-list-item-action {
        display: flex;
        justify-content: flex-end;
    }

    .remove {
        margin-right: 10px;
        height: 30px;
        background-color: #4f81bd;
    }

    .edit {
        height: 30px;
        background-color: #4f81bd;
    }
</style>

<body>
    <?php
    session_start();
    $_SESSION['khoa'] = '';
    $_SESSION['tukhoa'] = '';

    if (!empty($_POST['search'])) {
        $_SESSION['khoa'] = isset($_POST['khoa']) ? $_POST['khoa'] : '';
        $_SESSION['tukhoa'] = isset($_POST['tukhoa']) ? $_POST['tukhoa'] : '';
    }
    if (!empty($_POST['delete-search'])) {
        $_SESSION['khoa'] = '';
        $_SESSION['tukhoa'] = '';
    }
    ?>
    <div class="container">
        <form action="" method="POST" id="form" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-child">
                    <label class="form-text">
                        Khoa
                    </label>
                    <div class="sub-form-text">
                        <select id="khoa" name="khoa" id="khoa" style="height: inherit">
                            <?php
                            $khoa = array('0' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                            foreach ($khoa as $key => $value) {
                                if ($key == $_SESSION['khoa']) {
                                    echo '<option value="' . $key . '" selected="selected">' . $value . '</option>';
                                } else {
                                    echo '<option value="' . $key . '">' . $value . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-child">
                    <label class="form-text">Từ khoá</label>
                    <div class="sub-form-text">
                        <?php
                        echo '<input class="input" name="tukhoa" id="tukhoa" type="text" value="' . $_SESSION['tukhoa'] . '" />'
                        ?>
                    </div>
                </div>
            </div>
            <div class="search">
                <button type="button" name="delete-search" id="btn-delete" class="delete-search" onclick="Delete();">Xóa</button>
                <input type="submit" class="sub-search" name="search" value="Tìm kiếm" />
            </div>
            <div class="amount">
                <p>Số sinh viên tìm thấy: XXX</p>
            </div>
            <div class="add">
                <input type="submit" class="sub-add" name="add" value="Thêm" />
            </div>
            <table style="width: 550px;">
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th colspan="2">Action</th>
                </tr>
                <tr class="tr">
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td><button class="remove">Xóa</button></td>
                    <td><button class="edit">Sửa</button></td>
                </tr>
                <tr class="tr">
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td><button class="remove">Xóa</button></td>
                    <td><button class="edit">Sửa</button></td>
                </tr>
                <tr class="tr">
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td><button class="remove">Xóa</button></td>
                    <td><button class="edit">Sửa</button></td>
                </tr>
                <tr class="tr">
                    <td>4</td>
                    <td>Đinh Quuang D</td>
                    <td>Khoa học vật liệu</td>
                    <td><button class="remove">Xóa</button></td>
                    <td><button class="edit">Sửa</button></td>
                </tr>

            </table>
        </form>
    </div>
    <script>
        function Delete() {
            document.getElementById("khoa").value = '';
            document.getElementById("tukhoa").value = '';
        }
    </script>
</body>

</html>